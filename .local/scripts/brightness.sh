#!/usr/bin/env bash
brightness=$(ddcutil --skip-ddc-checks --noverify --bus 5 getvcp x10 | grep -oP 'current value\s*=\s*\K\d+')

if [ $brightness -eq 100 ]; then
  ddcutil --skip-ddc-checks --noverify --bus 5 setvcp 10 50
else
  ddcutil --skip-ddc-checks --noverify --bus 5 setvcp 10 100
fi

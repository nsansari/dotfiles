#!/usr/bin/env bash

# selected=$(rofi -show filebrowser ~/Pictures/wallpapers)

# echo "you selected $selected"

selected=$(rofi -theme /home/saif/.config/rofi/launchers/type-2/style-7.rasi -show file-browser-extended -file-browser-dir "~/Pictures/wallpapers/" -file-browser-stdout 2>/dev/null)

echo "$selected" >~/.local/state/wallpaperpath

if [ -z "$selected" ]; then
  exit 1
fi

pkill swaybg
swaybg -i "$(cat ~/.local/state/wallpaperpath)"

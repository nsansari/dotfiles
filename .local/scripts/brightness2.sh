case $1 in # Case switch
up)
  # Increase brightness by $2 ( Second term )
  ddcutil --skip-ddc-checks --noverify --bus 5 setvcp 10 + $2
  # brightness=$((brightness + $2))
  ;;
down)
  # Decrease brightness by $2 ( Second term )
  ddcutil --skip-ddc-checks --noverify --bus 5 setvcp 10 - $2
  # brightness=$((brightness - $2))
  ;;
*) # Anything other than up or down
  echo 'invalid'
  exit 1
  ;;
esac # Case switch end

brightness=$(ddcutil --skip-ddc-checks --noverify --bus 5 getvcp x10 | grep -oP 'current value\s*=\s*\K\d+')

exec dunstify -a "brightness" -u low -r "9999" -t 2000 -h int:value:"$brightness" -i "notification-display-brightness" "Brightness" "${brightness}%"
# Notification

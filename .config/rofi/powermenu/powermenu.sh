#!/usr/bin/env bash

dir="$HOME/.config/rofi/powermenu/"

# Options
shutdown=''
reboot=''
lock=''
suspend=''
logout=''

# Rofi CMD
rofi_cmd() {
  rofi -dmenu \
    -theme ${dir}/powermenu.rasi
}

# Pass variables to rofi dmenu
run_rofi() {
  echo -e "$shutdown\n$reboot\n$lock\n$suspend\n$logout" | rofi_cmd
}

# Actions
chosen="$(run_rofi)"
case ${chosen} in
$shutdown)
  systemctl poweroff
  ;;
$reboot)
  systemctl reboot
  ;;
$lock)
  hyprlock
  ;;
$suspend)
  systemctl suspend
  ;;
$logout)
  hyprctl dispatch exit
  ;;
esac
